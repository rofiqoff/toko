package id.pipit.toko.model.barang;

public class RequestSave {

    String kode;
    String nama;
    String stok;
    String jual;
    String beli;

    public RequestSave(String kode, String nama, String stok, String jual, String beli) {
        this.kode = kode;
        this.nama = nama;
        this.stok = stok;
        this.jual = jual;
        this.beli = beli;
    }
}
