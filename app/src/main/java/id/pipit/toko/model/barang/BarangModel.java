package id.pipit.toko.model.barang;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class BarangModel implements Parcelable {

    @SerializedName("kode_bar")
    private String kode_bar;
    @SerializedName("nama_barang")
    private String nama_barang;
    @SerializedName("stok")
    private String stok;
    @SerializedName("harga_jual")
    private String harga_jual;
    @SerializedName("harga_beli")
    private String harga_beli;
    @SerializedName("create_at")
    private String create_at;

    public BarangModel() {
    }

    public BarangModel(String kode_bar, String nama_barang, String stok, String harga_jual, String harga_beli, String create_at) {
        this.kode_bar = kode_bar;
        this.nama_barang = nama_barang;
        this.stok = stok;
        this.harga_jual = harga_jual;
        this.harga_beli = harga_beli;
        this.create_at = create_at;
    }

    protected BarangModel(Parcel in) {
        kode_bar = in.readString();
        nama_barang = in.readString();
        stok = in.readString();
        harga_jual = in.readString();
        harga_beli = in.readString();
        create_at = in.readString();
    }

    public static final Creator<BarangModel> CREATOR = new Creator<BarangModel>() {
        @Override
        public BarangModel createFromParcel(Parcel in) {
            return new BarangModel(in);
        }

        @Override
        public BarangModel[] newArray(int size) {
            return new BarangModel[size];
        }
    };

    public String getKode_bar() {
        return kode_bar;
    }

    public void setKode_bar(String kode_bar) {
        this.kode_bar = kode_bar;
    }

    public String getNama_barang() {
        return nama_barang;
    }

    public void setNama_barang(String nama_barang) {
        this.nama_barang = nama_barang;
    }

    public String getStok() {
        return stok;
    }

    public void setStok(String stok) {
        this.stok = stok;
    }

    public String getHarga_jual() {
        return harga_jual;
    }

    public void setHarga_jual(String harga_jual) {
        this.harga_jual = harga_jual;
    }

    public String getHarga_beli() {
        return harga_beli;
    }

    public void setHarga_beli(String harga_beli) {
        this.harga_beli = harga_beli;
    }

    public String getCreate_at() {
        return create_at;
    }

    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(kode_bar);
        parcel.writeString(nama_barang);
        parcel.writeString(stok);
        parcel.writeString(harga_jual);
        parcel.writeString(harga_beli);
        parcel.writeString(create_at);
    }
}
