package id.pipit.toko.model.barang;

public class SaveModel {
    String message;

    public SaveModel() {
    }

    public SaveModel(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
