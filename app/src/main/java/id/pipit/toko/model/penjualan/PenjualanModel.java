package id.pipit.toko.model.penjualan;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class PenjualanModel implements Parcelable {

    @SerializedName("kode_barang") String kodeBarang;
    @SerializedName("nama_barang") String namaBarang;
    @SerializedName("kategori") String kategori;
    @SerializedName("nama_pelanggan") String namaPelanggan;
    @SerializedName("jumlahpembelian") String jumlahPembelian;
    @SerializedName("hargasatuan") String hargaSatuan;
    @SerializedName("hargatotal") String hargaTotal;
    @SerializedName("create_at") String createAt;

    public PenjualanModel() {
    }

    public PenjualanModel(String kodeBarang, String namaBarang, String kategori, String namaPelanggan, String jumlahPembelian, String hargaSatuan, String hargaTotal, String createAt) {
        this.kodeBarang = kodeBarang;
        this.namaBarang = namaBarang;
        this.kategori = kategori;
        this.namaPelanggan = namaPelanggan;
        this.jumlahPembelian = jumlahPembelian;
        this.hargaSatuan = hargaSatuan;
        this.hargaTotal = hargaTotal;
        this.createAt = createAt;
    }

    protected PenjualanModel(Parcel in) {
        kodeBarang = in.readString();
        namaBarang = in.readString();
        kategori = in.readString();
        namaPelanggan = in.readString();
        jumlahPembelian = in.readString();
        hargaSatuan = in.readString();
        hargaTotal = in.readString();
        createAt = in.readString();
    }

    public static final Creator<PenjualanModel> CREATOR = new Creator<PenjualanModel>() {
        @Override
        public PenjualanModel createFromParcel(Parcel in) {
            return new PenjualanModel(in);
        }

        @Override
        public PenjualanModel[] newArray(int size) {
            return new PenjualanModel[size];
        }
    };

    public String getKodeBarang() {
        return kodeBarang;
    }

    public void setKodeBarang(String kodeBarang) {
        this.kodeBarang = kodeBarang;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getNamaPelanggan() {
        return namaPelanggan;
    }

    public void setNamaPelanggan(String namaPelanggan) {
        this.namaPelanggan = namaPelanggan;
    }

    public String getJumlahPembelian() {
        return jumlahPembelian;
    }

    public void setJumlahPembelian(String jumlahPembelian) {
        this.jumlahPembelian = jumlahPembelian;
    }

    public String getHargaSatuan() {
        return hargaSatuan;
    }

    public void setHargaSatuan(String hargaSatuan) {
        this.hargaSatuan = hargaSatuan;
    }

    public String getHargaTotal() {
        return hargaTotal;
    }

    public void setHargaTotal(String hargaTotal) {
        this.hargaTotal = hargaTotal;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(kodeBarang);
        parcel.writeString(namaBarang);
        parcel.writeString(kategori);
        parcel.writeString(namaPelanggan);
        parcel.writeString(jumlahPembelian);
        parcel.writeString(hargaSatuan);
        parcel.writeString(hargaTotal);
        parcel.writeString(createAt);
    }
}
