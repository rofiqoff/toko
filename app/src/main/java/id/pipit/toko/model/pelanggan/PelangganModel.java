package id.pipit.toko.model.pelanggan;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class PelangganModel implements Parcelable {
    @SerializedName("id_pelanggan")
    String id_pelanggan;
    @SerializedName("nama")
    String nama;
    @SerializedName("nama_pelanggan")
    String nama_pelanggan;
    @SerializedName("alamat")
    String alamat;
    @SerializedName("nama_barang")
    String nama_barang;

    public PelangganModel() {
    }

    public PelangganModel(String id_pelanggan, String nama, String nama_pelanggan, String alamat, String nama_barang) {
        this.id_pelanggan = id_pelanggan;
        this.nama = nama;
        this.nama_pelanggan = nama_pelanggan;
        this.alamat = alamat;
        this.nama_barang = nama_barang;
    }

    protected PelangganModel(Parcel in) {
        id_pelanggan = in.readString();
        nama = in.readString();
        nama_pelanggan = in.readString();
        alamat = in.readString();
        nama_barang = in.readString();
    }

    public static final Creator<PelangganModel> CREATOR = new Creator<PelangganModel>() {
        @Override
        public PelangganModel createFromParcel(Parcel in) {
            return new PelangganModel(in);
        }

        @Override
        public PelangganModel[] newArray(int size) {
            return new PelangganModel[size];
        }
    };

    public String getId_pelanggan() {
        return id_pelanggan;
    }

    public void setId_pelanggan(String id_pelanggan) {
        this.id_pelanggan = id_pelanggan;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNama_pelanggan() {
        return nama_pelanggan;
    }

    public void setNama_pelanggan(String nama_pelanggan) {
        this.nama_pelanggan = nama_pelanggan;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNama_barang() {
        return nama_barang;
    }

    public void setNama_barang(String nama_barang) {
        this.nama_barang = nama_barang;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id_pelanggan);
        parcel.writeString(nama);
        parcel.writeString(nama_pelanggan);
        parcel.writeString(alamat);
        parcel.writeString(nama_barang);
    }
}
