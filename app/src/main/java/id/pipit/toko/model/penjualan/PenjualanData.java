package id.pipit.toko.model.penjualan;

public class PenjualanData {

    String namaBarang;
    String kodeBarang;
    String kategori;
    String jumlahPembelian;
    String hargaSatuan;
    String hargaTotal;
    String month;
    String date;

    public PenjualanData(String namaBarang, String kodeBarang, String kategori, String jumlahPembelian, String hargaSatuan, String hargaTotal, String month, String date) {
        this.namaBarang = namaBarang;
        this.kodeBarang = kodeBarang;
        this.kategori = kategori;
        this.jumlahPembelian = jumlahPembelian;
        this.hargaSatuan = hargaSatuan;
        this.hargaTotal = hargaTotal;
        this.month = month;
        this.date = date;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    public String getKodeBarang() {
        return kodeBarang;
    }

    public void setKodeBarang(String kodeBarang) {
        this.kodeBarang = kodeBarang;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getJumlahPembelian() {
        return jumlahPembelian;
    }

    public void setJumlahPembelian(String jumlahPembelian) {
        this.jumlahPembelian = jumlahPembelian;
    }

    public String getHargaSatuan() {
        return hargaSatuan;
    }

    public void setHargaSatuan(String hargaSatuan) {
        this.hargaSatuan = hargaSatuan;
    }

    public String getHargaTotal() {
        return hargaTotal;
    }

    public void setHargaTotal(String hargaTotal) {
        this.hargaTotal = hargaTotal;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}