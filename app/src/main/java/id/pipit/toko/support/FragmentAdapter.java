package id.pipit.toko.support;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class FragmentAdapter extends FragmentPagerAdapter {

    private final List<androidx.fragment.app.Fragment> mFragments = new ArrayList<>();
    private final List<String> mFragmentTitles = new ArrayList<>();
    boolean isShowTitle = true;

    public FragmentAdapter(FragmentManager fm, boolean isShowTitle) {
        super(fm);
        this.isShowTitle = isShowTitle;
    }

    public void addFragment(Fragment fragment, String title) {
        mFragments.add(fragment);
        mFragmentTitles.add(title);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(isShowTitle) {
            return mFragmentTitles.get(position);
        }else {
            return "";
        }
    }
}