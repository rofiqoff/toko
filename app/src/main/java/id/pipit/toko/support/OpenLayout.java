package id.pipit.toko.support;

import android.app.Activity;
import android.content.Intent;

import id.pipit.toko.model.barang.BarangModel;
import id.pipit.toko.view.detailbarang.DetailBarangActivity;
import id.pipit.toko.view.home.fragment.listbarang.model.ItemBarang;

public class OpenLayout {
    public static void detailBarang(Activity activity, BarangModel data) {
        Intent intent = new Intent(activity, DetailBarangActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("dataBarang", data);
        activity.startActivity(intent);
    }
}
