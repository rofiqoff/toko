package id.pipit.toko.support;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Utils {

    public static void showShortToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void showLongToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static String setFormatRupiah(int harga) {
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

        String s = formatRupiah.format((double) harga);

        return s.replace("Rp", "Rp ");
    }

    public static String setFormatRupiahWithoutRp(int harga) {
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        String s = formatRupiah.format((double) harga);

        return s.replace("Rp", "");
    }

    public static String setFormatDate(String input, String pattern, String formatResult) {
        String result = "";
        try {
            SimpleDateFormat patternInput = new SimpleDateFormat(pattern, new Locale("ID"));
            SimpleDateFormat patternResult = new SimpleDateFormat(formatResult, new Locale("ID"));

            Date dateInput = patternInput.parse(input);

            patternResult.format(dateInput);

            result = patternResult.format(dateInput);

        } catch (Exception e) {
            e.printStackTrace();
            result = "";
        }

        return result;
    }

}
