package id.pipit.toko.api;

import java.util.List;

import id.pipit.toko.model.barang.BarangModel;
import id.pipit.toko.model.barang.RequestSave;
import id.pipit.toko.model.barang.SaveModel;
import id.pipit.toko.model.pelanggan.PelangganModel;
import id.pipit.toko.model.penjualan.PenjualanModel;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiInterface {

    @GET("/barang.php")
    Call<List<BarangModel>> getAllBarang();

    @POST("/barang.php")
    Call<SaveModel> saveData(@Body RequestSave request);

    @GET("/pelanggan.php")
    Call<List<PelangganModel>> getAllPelanggan();

    @GET("/detailpenjualan.php")
    Call<List<PenjualanModel>> detailPenjualan();

}
