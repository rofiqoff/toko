package id.pipit.toko.view.home.fragment.evaluasi;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IPieDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.util.ArrayList;
import java.util.List;

import id.pipit.toko.R;
import id.pipit.toko.api.ApiClient;
import id.pipit.toko.api.ApiInterface;
import id.pipit.toko.model.penjualan.PenjualanData;
import id.pipit.toko.model.penjualan.PenjualanModel;
import id.pipit.toko.support.ColorTemplateU;
import id.pipit.toko.support.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EvaluasiFragment extends Fragment implements OnChartValueSelectedListener {

    private Spinner spinner;
    private PieChart pieChart;
    private HorizontalBarChart barChart;
    private ProgressBar loading;
    private FrameLayout parentSpinner;

    private ArrayList<PenjualanData> dataPenjualan;

    public EvaluasiFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_evaluasi, container, false);
        spinner = view.findViewById(R.id.spinner_month);
        pieChart = view.findViewById(R.id.pie_chart);
        barChart = view.findViewById(R.id.bar_chart);
        pieChart.setUsePercentValues(true);
        pieChart.getDescription().setEnabled(false);

        loading = view.findViewById(R.id.loading);
        parentSpinner = view.findViewById(R.id.parent_spinner);

        initView();

        return view;
    }

    private void initView() {
        dataPenjualan = new ArrayList<>();

        initSpinner();
        getDataPenjualan();
    }

    private void initSpinner() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.month_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String name = parent.getItemAtPosition(position).toString();

                ArrayList<PenjualanData> dataSelect = new ArrayList<>();

                for (int i = 0; i < dataPenjualan.size(); i++) {
                    String date = dataPenjualan.get(i).getMonth();
                    if (name.equals(date)) {
                        dataSelect.add(dataPenjualan.get(i));
                    }
                }

                initPieChart(dataSelect);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void initPieChart(ArrayList<PenjualanData> data) {

        List<PieEntry> value = new ArrayList<>();

        for (int i = 0; i < data.size(); i++) {
            Log.e("dataKodeList", "\n" + data.get(i).getJumlahPembelian());

            int count = Integer.parseInt(data.get(i).getJumlahPembelian());
            String name = data.get(i).getNamaBarang();

            String label = name + " (" + count + ")";

            value.add(new PieEntry(count, label));
        }

        PieDataSet pieDataSet = new PieDataSet(value, "");
        pieDataSet.setDrawValues(true);
        pieDataSet.setSliceSpace(3f);
        pieDataSet.setSelectionShift(5f);
        pieDataSet.setColors(ColorTemplateU.MATERIAL_COLORS);
        pieDataSet.setValueTextSize(10f);
        pieDataSet.setValueTextColor(ContextCompat.getColor(getContext(), android.R.color.white));

        PieData pieData = new PieData(pieDataSet);
        pieData.setValueFormatter(new PercentFormatter());
        pieData.setValueTextSize(11f);
        pieData.setValueTextColor(Color.WHITE);

        Description description = new Description();
        description.setText("");

        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleColor(Color.WHITE);
        pieChart.setTransparentCircleColor(Color.WHITE);
        pieChart.setTransparentCircleAlpha(110);

        pieChart.setHoleRadius(38f);
        pieChart.setTransparentCircleRadius(51f);

        pieChart.setDrawCenterText(false);

//        pieChart.setRotationEnabled(false);
        pieChart.setHighlightPerTapEnabled(true);

        pieChart.setDrawEntryLabels(false);

//        pieChart.setMaxAngle(180f); // HALF CHART
//        pieChart.setRotationAngle(180f);
        pieChart.setCenterTextOffset(0, -20);

        pieChart.setDescription(description);
        pieChart.animateXY(1000, 1000);

        Legend legend = pieChart.getLegend();
        legend.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        legend.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        legend.setForm(Legend.LegendForm.CIRCLE);
        legend.setWordWrapEnabled(true);
        legend.setDrawInside(true);
        legend.setXEntrySpace(4f);


        pieChart.setData(pieData);
        pieChart.invalidate();
    }

    private void initBarChart(ArrayList<PenjualanData> data) {

        barChart.setOnChartValueSelectedListener(this);
        // chart.setHighlightEnabled(false);

        barChart.setDrawBarShadow(false);

        barChart.setDrawValueAboveBar(true);

        barChart.getDescription().setEnabled(false);

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        barChart.setMaxVisibleValueCount(60);

        // scaling can now only be done on x- and y-axis separately
        barChart.setPinchZoom(false);

        // draw shadows for each bar that show the maximum value
        // chart.setDrawBarShadow(true);

        barChart.setDrawGridBackground(false);

        XAxis xl = barChart.getXAxis();
        xl.setPosition(XAxis.XAxisPosition.BOTTOM);
//        xl.setTypeface(tfLight);
        xl.setDrawAxisLine(true);
        xl.setDrawGridLines(false);
        xl.setGranularity(10f);

        YAxis yl = barChart.getAxisLeft();
//        yl.setTypeface(tfLight);
        yl.setDrawAxisLine(true);
        yl.setDrawGridLines(true);
        yl.setAxisMinimum(0f); // this replaces setStartAtZero(true)
//        yl.setInverted(true);

        YAxis yr = barChart.getAxisRight();
//        yr.setTypeface(tfLight);
        yr.setDrawAxisLine(true);
        yr.setDrawGridLines(false);
        yr.setAxisMinimum(0f); // this replaces setStartAtZero(true)
//        yr.setInverted(true);

        barChart.setFitBars(true);
        barChart.animateY(2500);

        // setting data
//        seekBarY.setProgress(50);
//        seekBarX.setProgress(12);

        Legend l = barChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setFormSize(8f);
        l.setXEntrySpace(4f);


    }

    private void getDataPenjualan() {
        showLoading();

        ApiInterface apiInterface = ApiClient.getRetrofitInstance().create(ApiInterface.class);
        Call<List<PenjualanModel>> dataPenjualan = apiInterface.detailPenjualan();
        dataPenjualan.enqueue(new Callback<List<PenjualanModel>>() {
            @Override
            public void onResponse(Call<List<PenjualanModel>> call, Response<List<PenjualanModel>> response) {
                hideLoading();

                ArrayList<PenjualanModel> data = (ArrayList<PenjualanModel>) response.body();
                initData(data);
            }

            @Override
            public void onFailure(Call<List<PenjualanModel>> call, Throwable t) {
                hideLoading();

                String message = t.getMessage();
                Log.e("penjualan", "failed : " + message, t);
            }
        });
    }

    private void initData(ArrayList<PenjualanModel> data) {
        try {
            for (int i = 0; i < data.size(); i++) {
                mappingData(data.get(i));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void mappingData(PenjualanModel data) {
        String createAt = data.getCreateAt().trim();
        String patternDate = "yyyy-MM-dd";

        String namaBarang = data.getNamaBarang();
        String kodeBarang = data.getKodeBarang();
        String kategori = data.getKategori();
        String jumlah = data.getJumlahPembelian();
        String hargaSatuan = data.getHargaSatuan();
        String hargaTotal = data.getHargaTotal();

        String month = Utils.setFormatDate(createAt, patternDate, "MMMM");
        String date = Utils.setFormatDate(createAt, patternDate, "dd MMMM yyyy");

        PenjualanData dataMapping = new PenjualanData(namaBarang, kodeBarang, kategori, jumlah, hargaSatuan, hargaTotal, month, date);
        dataPenjualan.add(dataMapping);

    }

    private void showLoading() {
        loading.setVisibility(View.VISIBLE);
        parentSpinner.setVisibility(View.GONE);
        pieChart.setVisibility(View.GONE);
    }

    private void hideLoading() {
        loading.setVisibility(View.GONE);
        parentSpinner.setVisibility(View.VISIBLE);
        pieChart.setVisibility(View.VISIBLE);
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }
}
