package id.pipit.toko.view.home.fragment.listbarang.listener;

import id.pipit.toko.model.barang.BarangModel;

public interface ListBarangListener {
    void onSelectedItem(BarangModel data);
}
