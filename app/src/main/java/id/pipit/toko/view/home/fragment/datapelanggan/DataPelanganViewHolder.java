package id.pipit.toko.view.home.fragment.datapelanggan;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import id.pipit.toko.R;
import id.pipit.toko.model.pelanggan.PelangganModel;

public class DataPelanganViewHolder extends RecyclerView.ViewHolder {
    private TextView mNamaPelanggan;
    private TextView mAlamatPelanggan;
    private TextView mNamaKaryawan;
    private TextView mNamaBarang;

    public DataPelanganViewHolder(@NonNull View itemView) {
        super(itemView);

        mNamaPelanggan = itemView.findViewById(R.id.nama);
        mAlamatPelanggan = itemView.findViewById(R.id.alamat);
        mNamaKaryawan = itemView.findViewById(R.id.nama_karyawan);
        mNamaBarang = itemView.findViewById(R.id.nama_barang);
    }

    public void onBind(PelangganModel data) {
        String namaPelanggan = data.getNama_pelanggan().trim();
        String alamatPelanggan = data.getAlamat().trim();
        String namaKaryawan = data.getNama().trim();
        String namaBarang = data.getNama_barang().trim();

        mNamaPelanggan.setText(namaPelanggan);
        mAlamatPelanggan.setText(alamatPelanggan);
        mNamaKaryawan.setText(namaKaryawan);
        mNamaBarang.setText(namaBarang);
    }
}
