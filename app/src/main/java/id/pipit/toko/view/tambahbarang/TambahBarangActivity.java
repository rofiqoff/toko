package id.pipit.toko.view.tambahbarang;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.google.android.material.textfield.TextInputEditText;

import id.pipit.toko.R;
import id.pipit.toko.api.ApiClient;
import id.pipit.toko.api.ApiInterface;
import id.pipit.toko.model.barang.RequestSave;
import id.pipit.toko.model.barang.SaveModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahBarangActivity extends AppCompatActivity {

    private TextInputEditText mKodeBarang;
    private TextInputEditText mNamaBarang;
    private TextInputEditText mStokBarang;
    private TextInputEditText mHargaJualBarang;
    private TextInputEditText mHargaBeliBarang;
    private AppCompatButton mButtonSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_barang);
        initView();
    }

    private void initView() {
        mKodeBarang = findViewById(R.id.edit_text_kode_barang);
        mNamaBarang = findViewById(R.id.edit_text_nama_barang);
        mStokBarang = findViewById(R.id.edit_text_stok_barang);
        mHargaJualBarang = findViewById(R.id.edit_text_harga_jual_barang);
        mHargaBeliBarang = findViewById(R.id.edit_text_harga_beli_barang);
        mButtonSave = findViewById(R.id.button_simpan);

        mButtonSave.setOnClickListener(view -> {
            getValue();
        });
    }

    private void getValue() {
        String kode = mKodeBarang.getText().toString();
        String nama = mNamaBarang.getText().toString();
        String stok = mStokBarang.getText().toString();
        String jual = mHargaJualBarang.getText().toString();
        String beli = mHargaBeliBarang.getText().toString();

        requestSave(kode, nama, stok, jual, beli);
    }

    private void requestSave(String kode, String nama, String stok, String jual, String beli) {
        RequestSave request = new RequestSave(kode,nama,stok,jual,beli);

        ApiInterface apiInterface = ApiClient.getRetrofitInstance().create(ApiInterface.class);
        Call<SaveModel> call = apiInterface.saveData(request);
        call.enqueue(new Callback<SaveModel>() {
            @Override
            public void onResponse(Call<SaveModel> call, Response<SaveModel> response) {
                String message = response.body().getMessage();

                if (message.equals("Data inserted")){
                    Toast.makeText(getApplicationContext(), "Berhasil menyimpan", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Gagal menyimpan", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SaveModel> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Gagal menyimpan", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
