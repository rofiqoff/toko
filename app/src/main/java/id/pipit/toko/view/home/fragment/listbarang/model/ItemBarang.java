package id.pipit.toko.view.home.fragment.listbarang.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ItemBarang implements Parcelable {

    private int code;
    private String nama;
    private int harga;
    private int image;
    private String keterangan;

    public ItemBarang(int code, String nama, int harga, int image, String keterangan) {
        this.code = code;
        this.nama = nama;
        this.harga = harga;
        this.image = image;
        this.keterangan = keterangan;
    }

    protected ItemBarang(Parcel in) {
        code = in.readInt();
        nama = in.readString();
        harga = in.readInt();
        image = in.readInt();
        keterangan = in.readString();
    }

    public static final Creator<ItemBarang> CREATOR = new Creator<ItemBarang>() {
        @Override
        public ItemBarang createFromParcel(Parcel in) {
            return new ItemBarang(in);
        }

        @Override
        public ItemBarang[] newArray(int size) {
            return new ItemBarang[size];
        }
    };

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(code);
        dest.writeString(nama);
        dest.writeInt(harga);
        dest.writeInt(image);
        dest.writeString(keterangan);
    }
}
