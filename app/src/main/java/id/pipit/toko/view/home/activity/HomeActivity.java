package id.pipit.toko.view.home.activity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.navigation.NavigationView;

import id.pipit.toko.R;
import id.pipit.toko.support.FragmentAdapter;
import id.pipit.toko.support.Utils;
import id.pipit.toko.view.home.fragment.datapelanggan.DataPelangganFragment;
import id.pipit.toko.view.home.fragment.evaluasi.EvaluasiFragment;
import id.pipit.toko.view.home.fragment.history.HistoryFragment;
import id.pipit.toko.view.home.fragment.listbarang.view.ListBarangFragment;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawer;
    private Toolbar toolbar;
    private FrameLayout container;
    private FragmentAdapter fragmentAdapter;
    private MenuItem menuBarangKeluar;

    private boolean isShowMenuBarangKeluar = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initView();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        menuBarangKeluar = menu.findItem(R.id.menu_barang_keluar);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menuBarangKeluar.setVisible(isShowMenuBarangKeluar);
        return super.onPrepareOptionsMenu(menu);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_evaluasi:
                getSupportActionBar().setTitle("Evaluasi");
                isShowMenuBarangKeluar = false;
                invalidateOptionsMenu();
                displayView(0);
                break;
            case R.id.nav_list_barang:
                getSupportActionBar().setTitle("List Barang");
                isShowMenuBarangKeluar = true;
                invalidateOptionsMenu();
                displayView(1);
                break;
            case R.id.nav_data_pelanggan:
                getSupportActionBar().setTitle("Data Pelanggan");
                isShowMenuBarangKeluar = false;
                invalidateOptionsMenu();
                displayView(2);
                break;
//            case R.id.nav_history:
//                getSupportActionBar().setTitle("History");
//                isShowMenuBarangKeluar = false;
//                invalidateOptionsMenu();
//                displayView(3);
//                break;
//            case R.id.nav_logout:
//                Utils.showShortToast(getBaseContext(), "Logout");
//                break;
            default:
                getSupportActionBar().setTitle("Evaluasi");
                isShowMenuBarangKeluar = false;
                invalidateOptionsMenu();
                displayView(0);
                break;
        }

        drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void initView() {
        initToolbar();
        initDrawer();
        initAdapter();
    }

    private void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Evaluasi");
    }

    private void initDrawer() {
        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_evaluasi);
    }

    private void initAdapter() {
        container = findViewById(R.id.container);

        fragmentAdapter = new FragmentAdapter(getSupportFragmentManager(), false);
        fragmentAdapter.addFragment(new EvaluasiFragment(), "Evaluasi");
        fragmentAdapter.addFragment(new ListBarangFragment(), "List Barang");
        fragmentAdapter.addFragment(new DataPelangganFragment(), "Data Pelanggan");
        fragmentAdapter.addFragment(new HistoryFragment(), "History");

        FragmentTransaction manager = getSupportFragmentManager().beginTransaction();
        manager.add(container.getId(), fragmentAdapter.getItem(0)).commit();
    }

    private void displayView(int position) {
        FragmentTransaction manager = getSupportFragmentManager().beginTransaction();
        manager.replace(container.getId(), fragmentAdapter.getItem(position)).commit();
    }
}
