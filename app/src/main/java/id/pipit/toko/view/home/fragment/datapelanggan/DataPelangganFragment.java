package id.pipit.toko.view.home.fragment.datapelanggan;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import id.pipit.toko.R;
import id.pipit.toko.api.ApiClient;
import id.pipit.toko.api.ApiInterface;
import id.pipit.toko.model.barang.BarangModel;
import id.pipit.toko.model.pelanggan.PelangganModel;
import id.pipit.toko.support.Adapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataPelangganFragment extends Fragment {

    private RecyclerView listPelanggan;
    private ProgressBar loading;

    public DataPelangganFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_data_pelanggan, container, false);
        listPelanggan = view.findViewById(R.id.list_pelanggan);
        loading = view.findViewById(R.id.loading);

        initView();
        return view;
    }

    private void initView(){
        getAllPelanggan();
    }

    private void initAdapter(ArrayList<PelangganModel> list) {

        Adapter adapter = new Adapter<PelangganModel, DataPelanganViewHolder>(
                R.layout.item_list_pelanggan, DataPelanganViewHolder.class,
                PelangganModel.class, list) {
            @Override
            protected void bindView(DataPelanganViewHolder holder, PelangganModel model, int position) {
                holder.onBind(model);
            }
        };

        listPelanggan.setAdapter(adapter);
    }

    private void getAllPelanggan() {
        loading.setVisibility(View.VISIBLE);

        ApiInterface client = ApiClient.getRetrofitInstance().create(ApiInterface.class);
        Call<List<PelangganModel>> call = client.getAllPelanggan();
        call.enqueue(new Callback<List<PelangganModel>>() {
            @Override
            public void onResponse(Call<List<PelangganModel>> call, Response<List<PelangganModel>> response) {

                loading.setVisibility(View.GONE);

                ArrayList<PelangganModel> data = (ArrayList<PelangganModel>) response.body();

                initAdapter(data);
            }

            @Override
            public void onFailure(Call<List<PelangganModel>> call, Throwable t) {
                loading.setVisibility(View.GONE);

                String message = t.getMessage();
                Log.e("failed get barang", message);

                Toast.makeText(getContext(), "", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
