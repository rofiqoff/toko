package id.pipit.toko.view.detailbarang;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import id.pipit.toko.R;
import id.pipit.toko.model.barang.BarangModel;
import id.pipit.toko.support.Utils;
import id.pipit.toko.view.home.fragment.listbarang.model.ItemBarang;

public class DetailBarangActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_barang);
        initView();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Detail Barang");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initView() {
        initToolbar();

        ImageView imageBarang = findViewById(R.id.image_detail);
        TextView namaBarang = findViewById(R.id.text_nama_barang);
        TextView hargaBarang = findViewById(R.id.text_harga_barang);
        TextView deskripiBarang = findViewById(R.id.text_deskripsi);

        BarangModel data = getIntent().getParcelableExtra("dataBarang");

        String harga = Utils.setFormatRupiah(Integer.parseInt(data.getHarga_jual()));

        imageBarang.setImageResource(R.drawable.thumbnail);
        namaBarang.setText(data.getNama_barang());
        hargaBarang.setText(harga);
//        deskripiBarang.setText(data.getKeterangan());
    }
}