package id.pipit.toko.view.home.fragment.listbarang.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import id.pipit.toko.R;
import id.pipit.toko.api.ApiClient;
import id.pipit.toko.api.ApiInterface;
import id.pipit.toko.model.barang.BarangModel;
import id.pipit.toko.support.Adapter;
import id.pipit.toko.support.OpenLayout;
import id.pipit.toko.view.home.fragment.listbarang.listener.ListBarangListener;
import id.pipit.toko.view.home.fragment.listbarang.viewholder.ListItemBarangViewHolder;
import id.pipit.toko.view.tambahbarang.TambahBarangActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListBarangFragment extends Fragment implements ListBarangListener {

    private FloatingActionButton addBarang;
    private RecyclerView listBarang;
    private ProgressBar loading;

    public ListBarangFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list_barang, container, false);
        addBarang = view.findViewById(R.id.add_barang);
        listBarang = view.findViewById(R.id.list_barang);
        loading = view.findViewById(R.id.loading);

        initView();
        return view;
    }

    @Override
    public void onSelectedItem(BarangModel data) {
        OpenLayout.detailBarang(getActivity(), data);
    }

    private void initView() {
        addBarang.setOnClickListener(v -> {
            startActivity(new Intent(getContext(), TambahBarangActivity.class));
        });
    }

    private void initAdapter(ArrayList<BarangModel> list) {
        try {
            Adapter adapter = new Adapter<BarangModel, ListItemBarangViewHolder>(
                    R.layout.item_list_barang, ListItemBarangViewHolder.class,
                    BarangModel.class, list) {
                @Override
                protected void bindView(ListItemBarangViewHolder holder, BarangModel model, int position) {
                    holder.onBind(model, ListBarangFragment.this);
                }
            };

            listBarang.setAdapter(adapter);
            listBarang.setLayoutManager(new GridLayoutManager(getContext(), 2));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getAllBarang();
    }

    private void getAllBarang() {
        loading.setVisibility(View.VISIBLE);

        ApiInterface client = ApiClient.getRetrofitInstance().create(ApiInterface.class);
        Call<List<BarangModel>> call = client.getAllBarang();
        call.enqueue(new Callback<List<BarangModel>>() {
            @Override
            public void onResponse(Call<List<BarangModel>> call, Response<List<BarangModel>> response) {

                loading.setVisibility(View.GONE);

                ArrayList<BarangModel> data = (ArrayList<BarangModel>) response.body();

                initAdapter(data);
            }

            @Override
            public void onFailure(Call<List<BarangModel>> call, Throwable t) {
                loading.setVisibility(View.GONE);

                String message = t.getMessage();
                Log.e("failed get barang", message);

                Toast.makeText(getContext(), "", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
