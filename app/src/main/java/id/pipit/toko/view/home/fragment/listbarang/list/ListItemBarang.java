package id.pipit.toko.view.home.fragment.listbarang.list;

import java.util.ArrayList;

import id.pipit.toko.R;
import id.pipit.toko.view.home.fragment.listbarang.model.ItemBarang;

public class ListItemBarang {
    public static ArrayList<ItemBarang> getListBarang() {
        ArrayList<ItemBarang> data = new ArrayList<>();

        data.add(new ItemBarang(1, "Panci", 7000, R.drawable.panci, ""));
        data.add(new ItemBarang(2, "Kompor Gas", 120000, R.drawable.komporgas, ""));
        data.add(new ItemBarang(3, "Wajan", 30000, R.drawable.penggorengan, ""));
        data.add(new ItemBarang(4, "Sendok", 15000, R.drawable.sendok, ""));
        data.add(new ItemBarang(5, "Panci", 7000, R.drawable.panci, ""));
        data.add(new ItemBarang(6, "Kompor Gas", 120000, R.drawable.komporgas, ""));
        data.add(new ItemBarang(7, "Wajan", 30000, R.drawable.penggorengan, ""));
        data.add(new ItemBarang(8, "Sendok", 15000, R.drawable.sendok, ""));
        data.add(new ItemBarang(9, "Panci", 7000, R.drawable.panci, ""));
        data.add(new ItemBarang(10, "Kompor Gas", 120000, R.drawable.komporgas, ""));
        data.add(new ItemBarang(11, "Wajan", 30000, R.drawable.penggorengan, ""));
        data.add(new ItemBarang(12, "Sendok", 15000, R.drawable.sendok, ""));

        return data;
    }
}
