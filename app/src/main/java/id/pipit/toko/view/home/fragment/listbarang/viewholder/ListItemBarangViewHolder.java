package id.pipit.toko.view.home.fragment.listbarang.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import id.pipit.toko.R;
import id.pipit.toko.model.barang.BarangModel;
import id.pipit.toko.support.Utils;
import id.pipit.toko.view.home.fragment.listbarang.listener.ListBarangListener;

public class ListItemBarangViewHolder extends RecyclerView.ViewHolder {
    private CardView root;
    private ImageView image;
    private TextView nama;
    private TextView harga;

    public ListItemBarangViewHolder(@NonNull View itemView) {
        super(itemView);

        image = itemView.findViewById(R.id.image_content);
        nama = itemView.findViewById(R.id.nama_barang);
        harga = itemView.findViewById(R.id.harga_barang);

        root = itemView.findViewById(R.id.root);
    }

    public void onBind(final BarangModel data, final ListBarangListener listener) {
        String hargaJual = data.getHarga_jual();

        String hargaString = "";
        if (hargaJual != " ") {
            hargaString = Utils.setFormatRupiah(Integer.parseInt(hargaJual));
        }

        int imageDefault = R.drawable.thumbnail;

        image.setImageResource(imageDefault);
        nama.setText(data.getNama_barang().trim());
        harga.setText(hargaString);

        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onSelectedItem(data);
            }
        });
    }
}
